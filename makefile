install-dependencies:
	pip install -r requirements.txt

run:
	python src/main.py

test:
	pytest -vv -s


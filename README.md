# Battleship Game

- **Autores**: Douglas Vedovelli e Ricardo Lampert
- **Linguagem**: Python

# Ideia

- Aplicação pratica dos principais conceitos e ferramentas do desenvolvimento de software

# Tecnologias utilizadas

- Python
- Git
- Gerenciador de ambiente
- Ambiente virtual
- Testes
- CI/CD
- Banco de dados
- Docker
- RestAPI

# Conteudos vistos

- Git: Versionamento, CI/CD e etc
- Gerenciamento de ambiente: Uso da ferramenta pyenv
- Banco de dados: SQL e o uso da ferramenta do DBeaver
- Docker: Dockerização
- RestAPI: Criação de um modelo cliente servidor

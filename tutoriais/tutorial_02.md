# 2. Criação das funções referentes a inicialização do tabuleiro e criação do arquivo de CI/CD
---
---
## Criação de uma nova branch

1. Garantir que a branch atual é a master com o comando `git checkout master`
2. Atualizar o repositório com o comando `git pull`
3. Criar uma branch inicial com o comando `git checkout -b <nome da branch>`
---
## Criação do arquivo `tabuleiro.py`
1. Em `src`, criar o arquivo `tabuleiro.py`
2. Criar as funções `cria_tabuleiro` e `le_tabuleiro_completo`. 
    - A função `cria_tabuleiro` recebe 2 parametros, `linhas` e `colunas`. Ela retorna uma matriz de tamanho `linhas` x `colunas` com valor 0
    - A função `le_tabuleiro_completo` recebe duas matrizes de tamanho `linha` x `coluna` e retorna uma string com as matrizes lado a lado
---
## Exemplo da função `cria_tabuleiro` no arquivo `tabuleiro.py`:
```
def cria_tabuleiro(linhas, colunas):
    tabuleiro = []
    for _ in range(linhas):
        linha = []
        for _ in range(colunas):
            linha.append(0)
        tabuleiro.append(linha)
    return tabuleiro
```
---
## Exemplo da função `le_tabuleiro_completo` no arquivo `tabuleiro.py`:
```
def le_tabuleiro_completo(tabuleiro_1, tabuleiro_2):
    tabuleiro_completo=""
    n_tabuleiro_1 = len(tabuleiro_1[0]) if tabuleiro_1 else 0
    n_tabuleiro_2 = len(tabuleiro_2[0]) if tabuleiro_2 else 0
    tamanho_total = n_tabuleiro_1*2+n_tabuleiro_2*2+3
    tabuleiro_completo = f"{'BATTLESHIP GAME':^{tamanho_total}}\n\n"
    tabuleiro_completo += f"{'Jogador 1':{n_tabuleiro_1*2}}   {'Jogador 2':{n_tabuleiro_2*2}}\n\n"
    for linha, linha_adversario in zip(tabuleiro_1,tabuleiro_2):
        for coluna in linha:
            tabuleiro_completo += f"{coluna} "
        tabuleiro_completo += "   "
        for coluna in linha_adversario:
            tabuleiro_completo += f"{coluna} "
        tabuleiro_completo += "\n"
    return tabuleiro_completo
```
---
## Modificação do `main.py` para chamar as funções de criação e exibição do tabuleiro
1. Deletar a linha de print do hello world
2. Adicionar o import do `tabuleiro.py` e suas funções
3. Setar as variaveis `linhas` e `colunas`
4. Chamar as funções `cria_tabuleiro` e `exibe_tabuleiro`.
5. Rodar o programa com o comando `make run`
---
## Exemplo das alterações na função `main`:
```
from tabuleiro import cria_tabuleiro, le_tabuleiro_completo

linhas=10
colunas=10
tabuleiro = cria_tabuleiro(linhas,colunas)
tabuleiro_adversario = cria_tabuleiro(linhas,colunas)
print(le_tabuleiro_completo(tabuleiro, tabuleiro_adversario))
```
---
## Criação de testes para as funções de `tabuleiro`:
1. Criar uma nova pasta `tests`
2. Dentro da pasta `tests`, criar o arquivo `test_tabuleiro`
3. No arquivo, criar as funções `test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro` e `test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string`
    - A função `test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro` vai chamar a função cria tabuleiro passando diferentes dimensões e valores e comparar o retorno com um resultados esperados
    - A função `test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string` vai chamar a função le tabuleiro passadno dois tabuleiros e comparar o resultado com uma string esperada desses tabuleiros
4. Rodar os testes com o comando `pytest`
---
## Exemplo da função de teste `test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro` no arquivo `test_tabuleiro`:
```
from src.tabuleiro import cria_tabuleiro

def test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro():
    resultado = cria_tabuleiro(3,3)
    esperado = [[0,0,0],[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(2,3)
    esperado = [[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(3,2)
    esperado = [[0,0],[0,0],[0,0]]
    assert esperado == resultado
```
---
## Exemplo da função de teste `test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string` no arquivo `test_tabuleiro`:
```
from src.tabuleiro import le_tabuleiro_completo

def test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string():
    tabuleiro_1 = [[0,0,0],[0,0,0],[0,0,0]]
    tabuleiro_2 = [[1,2,3],[4,5,6],[7,8,9]]
    esperado = """BATTLESHIP GAME\n\nJogador 1   Jogador 2\n\n0 0 0    1 2 3 \n0 0 0    4 5 6 \n0 0 0    7 8 9 \n"""
    resultado = le_tabuleiro_completo(tabuleiro_1,tabuleiro_2)
    assert esperado == resultado
```
## Adição da chamada de teste e install-dependences no makefile

1. Adicionar ao arquivo `makefile` a tarefa `test` com o comando `pytest -vv -s`
2. Adicionar a tarefa `install-dependencies` com o comando `pip install -r requirements.txt`
2. Rodar os testes com o comando `make test`
---
## Exemplo dos jobs `test` e `install-dependencies` no arquivo `makefile`
```
test:
    pytest -vv -s

install_dependencies:
    pip install -r requirements.txt
```
---
## Criação do arquivo de `CI/CD`
1. Criar um arquivo chamado `.gitlab-ci.yml`
2. No arquivo, adicionar o estagio de `test` no campo `stages`
3. Adicionar os `requirements` com `.py_requirements: &py_requirements` onde dentro dele dever ter a imagem `python:latest` e na parte de `before_script` deve ir o comando `apt-get update --ignore-missing --fix-missing` para atualizar as dependencias e o comando `apt-get install -y make` para instalar o make
4. Adicionar o jobs `teste` que vai chamar o requirements com o comando `<<: *py_requirements`, participara do stage `teste` e chamará o script `make test`

---
## Exemplo de arquivo de `CI/CD`
```
stages:
  - teste

.py_requirements: &py_requirements
  image: python:latest
  before_script:
    - apt-get update --ignore-missing --fix-missing
    - apt-get install -y make
    - pip install -r requirements.txt

teste:
  <<: *py_requirements
  stage: teste
  script:
    - make test
```
---
## Adicionar alterações ao repositório git

1. Rever as alterações, pela própria IDE ou com um programa como `meld` com o comando `meld .`
2. Após rever as alterações, rodar o comando `git status`
3. Usar o comando `git add <arquivos para subir as alterações>`, caso sejam todos presente no git status, é possive utilizar o comando `git add -A`
4. Realizar o commit com o comando `commit -m "<Mensagem de commit>"`
5. Fazer o push com o comando `git push`
6. No gitlab, abrir o `Merge request`, acompanhar a `pipeline` que irá executar os testes de maneira automática e ,depois de revisado, realizar o `Merge`

# 3. Criação das funções referentes ao posicionamento dos navios
---
---
## Criação de uma nova branch

1. Garantir que a branch atual é a master com o comando `git checkout master`
2. Atualizar o repositório com o comando `git pull`
3. Criar uma branch inicial com o comando `git checkout -b <nome da branch>`
---
# Criação do arquivo `navios.py`

1. Em `src`, criar o arquivo `navios.py`
2. Criar as listas `NAVIOS`, `TAMANHOS` e `SIMBOLOS` com o nome dos navios, os tamanhos e o simbolo que será representado no tabuleiro respectivamente
3. Criar as funções `posiciona_navios`, `valida_posicao` e `adiciona_navio`
    - A função `posiciona_navios` recebe um tabuleiro como parametro e para cada navio, simbolo e tamanho nas listas criadas anteriormente ela vai entrar em um loop infinito, pedir ao usuario um `x`, um `y` e um `sentido` e com essas informações, vai chamar a função `valida_posicao`, se a posição for valida, chama a função `adiciona_navio` e sai do loop infinito, se não, volta para o começo do loop, no final, retorna `tabuleiro`
    - A função `valida_posicao` recebe como parametro as informações de `tabuleiro`, `SIMBOLOS`, `tamanho`, `x`, `y` e `sentido` e retorna `false` caso `x` ou `y` fora do tabuleiro, caso a `posição+tamanho` do navio naquele `sentido` ultrapasse o limite do tabuleiro ou se alguma posição que o navio vai ocupar ja possui algum dos simbolos da lista de simbolos e retorna `true` caso não caia em nenhum desses casos
    - A função `adiciona_navio` recebe `tabuleiro`, `simbolo` do navio atual, `tamanho`, `x`, `y` e `sentido` e vai substituir na posição `X,Y` até `x+tamanho` ou `y+tamanho`, dependendo do `sentido`, com o `simbolo` e retorna o tabuleiro atualizado
---
## Exemplo das listas no arquivo `navios.py`:
```
NAVIOS = [
    'porta_avioes',
    'cruzador',
    'torpedeiro',
    'rebocador',
    'submarino',
]
SIMBOLOS = ['p','c','t','r','s']
TAMANHOS = [5,4,3,2,1]
```
---
## Exemplo da função `posiciona_navios` no arquivo `navios.py`:
```
def posiciona_navios(tabuleiro):
    for navio, simbolo, tamanho in zip(NAVIOS,SIMBOLOS,TAMANHOS):
        while True:
            print(f"Posicionando navio {navio}")
            print("Posição:")
            x = int(input("x:"))
            y = int(input("y:"))
            sentido = input("Horizontal (h) ou vertical (v):")
            if valida_posicao(tabuleiro,SIMBOLOS, tamanho, x, y, sentido):
                tabuleiro = adiciona_navio(tabuleiro, simbolo, tamanho, x, y, sentido)
                break
    return tabuleiro
```
---

## Exemplo da função `valida_posicao` no arquivo `navios.py`:
```
def valida_posicao(tabuleiro, simbolos, tamanho, x, y, sentido):
    n_linhas = len(tabuleiro)
    n_colunas = len(tabuleiro[0])
    if x >= n_colunas or y >= n_linhas:
        input(f"Erro: Posição escolhida fora do tabuleiro")
        return False
    if sentido == "h":
        if x+tamanho > n_colunas:
            input(f"Erro: Navio ultrapassa os limites do tabuleiro")
            return False
        for i in range(tamanho):
            if tabuleiro[y][x+i] in simbolos:
                input(f"Erro: Navio sobrepões outro navio")
                return False
    else: 
        if y+tamanho > n_colunas:
            input(f"Erro: Navio ultrapassa os limites do tabuleiro")
            return False
        for i in range(tamanho):
            if tabuleiro[y+i][x] in simbolos:
                input(f"Erro: Navio sobrepões outro navio")
                return False
    return True
```
---

## Exemplo da função `adiciona_navio` no arquivo `navios.py`:
```
def adiciona_navio(tabuleiro, simbolo, tamanho, x, y, sentido):
    for i in range(tamanho):
        if sentido == "h":
            tabuleiro[y][x+i] = simbolo
        else:
            tabuleiro[y+i][x] = simbolo
    print("Navio_adicionado")
    return tabuleiro
```

---

## Modificação do `main.py` para chamar a função es de criação e exibição do tabuleiro
1. Adicionar a chamada da função `posiciona_navios(tabuleiro)` e salvar o resultado na variavel `tabuleiro` antes de printar o tabuleiro
2. Rodar o programa com o comando `make run`
---

## Exemplo de modificações no arquivo `main.py`:
```
from tabuleiro import cria_tabuleiro, le_tabuleiro_completo
from navios import posiciona_navios

linhas=10
colunas=10
tabuleiro = cria_tabuleiro(linhas,colunas)
tabuleiro_adversario = cria_tabuleiro(linhas,colunas)
tabuleiro = posiciona_navios(tabuleiro)
print(le_tabuleiro_completo(tabuleiro, tabuleiro_adversario))
```
---

## Modificação do `tabuleiro.py` para chamar adicionar uma função que printa um unico tabuleiro
1. Criar a função `le_tabuleiro_unico`
    - A função `le_tabuleiro_unico` recebe um tabuleiro e retorna em formato de string
2. Usar essa função para exibir os navios sendo inseridos
---

## Exemplo da função `le_tabuleiro_unico` no arquivo `tabuleiro.py`:
```
def le_tabuleiro_unico(tabuleiro):
    tabuleiro_completo=""
    n_tabuleiro= len(tabuleiro[0]) if tabuleiro else 0
    tamanho_total = n_tabuleiro*2+3
    tabuleiro_completo = f"{'BATTLESHIP GAME':^{tamanho_total}}\n\n"
    tabuleiro_completo += f"{'Jogador 1':{n_tabuleiro*2}}\n\n"
    for linha in tabuleiro:
        for coluna in linha:
            tabuleiro_completo += f"{coluna} "    
        tabuleiro_completo += "\n"
    return tabuleiro_completo

```
---

































## Criação de testes para as funções de `navios`:
1. Dentro da pasta `tests`, criar o arquivo `test_navios`
2. No arquivo, criar as funções `test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro` e `test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string`
    - A função `test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro` vai chamar a função cria tabuleiro passando diferentes dimensões e valores e comparar o retorno com um resultados esperados
    - A função `test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string` vai chamar a função le tabuleiro passadno dois tabuleiros e comparar o resultado com uma string esperada desses tabuleiros
4. Rodar os testes com o comando `make test`
---
## Exemplo de arquivo `test_navios`:
```
from src.tabuleiro import le_tabuleiro_completo, cria_tabuleiro

def test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro():
    resultado = cria_tabuleiro(3,3)
    esperado = [[0,0,0],[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(2,3)
    esperado = [[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(3,2)
    esperado = [[0,0],[0,0],[0,0]]
    assert esperado == resultado

def test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string():
    tabuleiro_1 = [[0,0,0],[0,0,0],[0,0,0]]
    tabuleiro_2 = [[1,2,3],[4,5,6],[7,8,9]]
    esperado = """BATTLESHIP GAME\n\nJogador 1   Jogador 2\n\n0 0 0    1 2 3 \n0 0 0    4 5 6 \n0 0 0    7 8 9 \n"""
    resultado = le_tabuleiro_completo(tabuleiro_1,tabuleiro_2)
    assert esperado == resultado
```
---
## Criação de testes para as funções de `navios`:
1. Dentro da pasta `tests`, criar o arquivo `test_navios`
2. No arquivo, criar as funções `test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro` e `test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string`
    - A função `test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro` vai chamar a função cria tabuleiro passando diferentes dimensões e valores e comparar o retorno com um resultados esperados
    - A função `test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string` vai chamar a função le tabuleiro passadno dois tabuleiros e comparar o resultado com uma string esperada desses tabuleiros
4. Rodar os testes com o comando `make test`
---
## Exemplo de arquivo `test_navios`:
```
from src.tabuleiro import le_tabuleiro_completo, cria_tabuleiro

def test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro():
    resultado = cria_tabuleiro(3,3)
    esperado = [[0,0,0],[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(2,3)
    esperado = [[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(3,2)
    esperado = [[0,0],[0,0],[0,0]]
    assert esperado == resultado

def test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string():
    tabuleiro_1 = [[0,0,0],[0,0,0],[0,0,0]]
    tabuleiro_2 = [[1,2,3],[4,5,6],[7,8,9]]
    esperado = """BATTLESHIP GAME\n\nJogador 1   Jogador 2\n\n0 0 0    1 2 3 \n0 0 0    4 5 6 \n0 0 0    7 8 9 \n"""
    resultado = le_tabuleiro_completo(tabuleiro_1,tabuleiro_2)
    assert esperado == resultado
```





---
---





















- Criação das jogadas
- criação do menu
- criação da organização das peças


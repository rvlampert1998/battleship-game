# 1. Criação do projeto, pré requisitos e preparação do ambiente:
---
---
## Pré-requisitos para desenvolvimento do projeto:

- git
- pyenv
- docker
- dbeaver
- insomnia
---
## Criação do projeto no gitlab:

1. Entrar na conta gitlab
2. Acessar a aba `Projects`
3. Selecionar `New project`
4. Selecionar `Create blank project`
5. Preencher as informações e selecionar `Create project`
---
## Clone do repositorio:

1. Entrar no repositório no gitlab
2. Selecionar `Clone`
3. Abrir uma janela do terminal
4. Navegar até a pasta onde o projeto será clonado com os comandos:
    - `mkdir` para criar uma nova pasta
    - `cd <nome da pasta>` para navegar entre elas
    - `cd ..` para acessar uma pasta mais externa a atual
5. Clonar o repositorio com o comando `git clone <ctrl+shift+v>`
6. Entrar na pasta com o comando `cd <nome do projeto>` e abrir o vscode com o comando `code .`
---
## Criando ambiente virtual:

1. Selecionar a versão do Python local com o comando `pyenv local <versão do python>`
2. Criar o ambiente virtual com o comando `python -m venv <nome do ambiente virtual>`
3. Ativar o ambiente virtual com o comando `. <nome do ambiente virtual>/bin/activate`
    - Para desativar o ambiente virtual, usar o comando `deactivate`
---

## Criação de uma branch

1. Criar uma branch inicial com o comando `git checkout -b <nome da branch>`

---
## Criação da main

1. Criar uma pasta chamada `src`
2. Dentro de `src`, criar um arquivo chamado `main.py`
3. Digitar o comando `print("hello world")` e salva o arquivo
4. Em um terminal, no diretorio `src` do projeto, rodar o programa com o comando `python main.py`
5. Deve exibir na tela a mensagem `hello world`
---
## Exemplo da função main
```
print("hello world")
```
---

## Criação do makefile

1. Criar na raiz do projeto um arquivo chamado `makefile`
2. Criar o script `run` adicionando a chamada do comando `python src/main.py`
3. Em um terminal, no diretorio raiz, rodar o programa com o comando `make run`
4. Deve rodar o programa normalmente
---
## Exemplo do job `run` no arquivo `makefile`
```
run:
	python src/main.py
```
---
## Criação do gitignore

1. Na raiz do projeto, criar um arquivo com o nome `.gitignore` que será responsavel por ignorar os arquivos que não irão para o repositorio git
2. Adicionar os seguintes itens ao arquivo `.venv`, `*/__pycache__` e `*.pytest_cache`
---
## Adicionar alterações ao repositório git

1. Rever as alterações, pela própria IDE ou com um programa como `meld` com o comando `meld .`
2. Após rever as alterações, rodar o comando `git status`
3. Usar o comando `git add <arquivos para subir as alterações>`, caso sejam todos presente no git status, é possive utilizar o comando `git add -A`
4. Realizar o commit com o comando `commit -m "<Mensagem de commit>"`
5. Fazer o push com o comando `git push`
6. No gitlab, abrir o `Merge request`, e depois de revisado, realizar o `Merge`
from tabuleiro import le_tabuleiro_unico

NAVIOS = [
    'porta_avioes',
    'cruzador',
    'torpedeiro',
    'rebocador',
    'submarino',
]
SIMBOLOS = ['p','c','t','r','s']
TAMANHOS = [5,4,3,2,1]

def posiciona_navios(tabuleiro):
    for navio, simbolo, tamanho in zip(NAVIOS,SIMBOLOS,TAMANHOS):
        print(le_tabuleiro_unico(tabuleiro))
        while True:
            print(f"Posicionando navio {navio}")
            print("Posição:")
            x = int(input("x:"))
            y = int(input("y:"))
            sentido = input("Horizontal (h) ou vertical (v):")
            if valida_posicao(tabuleiro,SIMBOLOS, tamanho, x, y, sentido):
                tabuleiro = adiciona_navio(tabuleiro, simbolo, tamanho, x, y, sentido)
                break
    return tabuleiro

def adiciona_navio(tabuleiro, simbolo, tamanho, x, y, sentido):
    for i in range(tamanho):
        if sentido == "h":
            tabuleiro[y][x+i] = simbolo
        else:
            tabuleiro[y+i][x] = simbolo
    print("Navio_adicionado")
    return tabuleiro

def valida_posicao(tabuleiro, simbolos, tamanho, x, y, sentido):
    n_linhas = len(tabuleiro)
    n_colunas = len(tabuleiro[0])
    if x >= n_colunas or y >= n_linhas:
        input(f"Erro: Posição escolhida fora do tabuleiro")
        return False
    if sentido == "h":
        if x+tamanho > n_colunas:
            input(f"Erro: Navio ultrapassa os limites do tabuleiro")
            return False
        for i in range(tamanho):
            if tabuleiro[y][x+i] in simbolos:
                input(f"Erro: Navio sobrepões outro navio")
                return False
    else: 
        if y+tamanho > n_colunas:
            input(f"Erro: Navio ultrapassa os limites do tabuleiro")
            return False
        for i in range(tamanho):
            if tabuleiro[y+i][x] in simbolos:
                input(f"Erro: Navio sobrepões outro navio")
                return False
    return True

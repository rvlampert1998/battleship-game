# ESTADOS = {
#     0:"agua",
#     1:"navio",
#     2:"acerto",
#     3:"erro"
# }

# def realiza_jogada(tabuleiro,x,y):
#     print(f"Jogou {x},{y}")
#     if tabuleiro[x][y] == 0:
#         tabuleiro[x][y] = 3
#         print("Errou")
#     elif tabuleiro[x][y] == 1:
#         tabuleiro[x][y] = 2
#         print("Acertou")
#     else:
#         raiseExceptions("Posição invalida")
#     return tabuleiro


def cria_tabuleiro(linhas, colunas):
    tabuleiro = []
    for _ in range(linhas):
        linha = []
        for _ in range(colunas):
            linha.append(0)
        tabuleiro.append(linha)
    return tabuleiro

def le_tabuleiro_completo(tabuleiro_1, tabuleiro_2):
    tabuleiro_completo=""
    n_tabuleiro_1 = len(tabuleiro_1[0]) if tabuleiro_1 else 0
    n_tabuleiro_2 = len(tabuleiro_2[0]) if tabuleiro_2 else 0
    tamanho_total = n_tabuleiro_1*2+n_tabuleiro_2*2+3
    tabuleiro_completo = f"{'BATTLESHIP GAME':^{tamanho_total}}\n\n"
    tabuleiro_completo += f"{'Jogador 1':{n_tabuleiro_1*2}}   {'Jogador 2':{n_tabuleiro_2*2}}\n\n"
    for linha, linha_adversario in zip(tabuleiro_1,tabuleiro_2):
        for coluna in linha:
            tabuleiro_completo += f"{coluna} "
        tabuleiro_completo += "   "
        for coluna in linha_adversario:
            tabuleiro_completo += f"{coluna} "
        tabuleiro_completo += "\n"
    return tabuleiro_completo

def le_tabuleiro_unico(tabuleiro):
    tabuleiro_completo=""
    n_tabuleiro= len(tabuleiro[0]) if tabuleiro else 0
    tamanho_total = n_tabuleiro*2+3
    tabuleiro_completo = f"{'BATTLESHIP GAME':^{tamanho_total}}\n\n"
    tabuleiro_completo += f"{'Jogador 1':{n_tabuleiro*2}}\n\n"
    for linha in tabuleiro:
        for coluna in linha:
            tabuleiro_completo += f"{coluna} "    
        tabuleiro_completo += "\n"
    return tabuleiro_completo
  
    
 
    
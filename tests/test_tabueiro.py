from src.tabuleiro import le_tabuleiro_completo, cria_tabuleiro

def test_cria_tabuleiro_deve_retornar_corretamente_o_tabuleiro():
    resultado = cria_tabuleiro(3,3)
    esperado = [[0,0,0],[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(2,3)
    esperado = [[0,0,0],[0,0,0]]
    assert esperado == resultado
    resultado = cria_tabuleiro(3,2)
    esperado = [[0,0],[0,0],[0,0]]
    assert esperado == resultado

def test_le_tabuleiro_completo_deve_retornar_tabuleiros_em_formato_de_string():
    tabuleiro_1 = [[0,0,0],[0,0,0],[0,0,0]]
    tabuleiro_2 = [[1,2,3],[4,5,6],[7,8,9]]
    esperado = """BATTLESHIP GAME\n\nJogador 1   Jogador 2\n\n0 0 0    1 2 3 \n0 0 0    4 5 6 \n0 0 0    7 8 9 \n"""
    resultado = le_tabuleiro_completo(tabuleiro_1,tabuleiro_2)
    assert esperado == resultado